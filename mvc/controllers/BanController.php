<?php
    class BanController extends Controller{
        public function getListBan(){
            $ban = $this->model("Ban");
            //var_dump($ban->getListBan());
            echo json_encode($ban->getListBan());
        }
        public function addBan(){
            $ten = "Bàn 11";
            $tinhtrang = 0;
            $ban = $this->model("Ban");
            if($ban->addBan($ten,$tinhtrang)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function deleteBan($id){
            $ban = $this->model("Ban");   
            if($ban->deleteBan($id)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function updateBan($id){
            $tinhtrang = 0;
            $ban = $this->model("Ban");   
            if($ban->updateTinhTrang($id,$tinhtrang)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
    }

?>