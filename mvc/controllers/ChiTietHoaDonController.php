<?php 
    class ChiTietHoaDonController extends Controller{
        public function getChiTietByIdHoaDon($idHoaDon){
            $chiTiet = $this->model("ChiTietHoaDon");
            if($chiTiet->getChiTietByIdHoaDon($idHoaDon) ==0){
                echo "not found";         
            }else{
                echo json_encode($chiTiet->getChiTietByIdHoaDon($idHoaDon));
            }
        }
        public function insertChiTiet(){
            $chiTiet = $this->model("ChiTietHoaDon");
            $maHoaDon =  $_POST["mahoadon"];
            $maThucUong = $_POST["mathucuong"];  
            $soLuong = $_POST["soluong"];
            if($chiTiet->insertChiTiet($maHoaDon,$maThucUong,$soLuong)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }
        public function getChiTietById($id){
            $chiTiet = $this->model("ChiTietHoaDon");
            if($chiTiet->getChiTietById($id) != null){
                echo json_encode($chiTiet->getChiTietById($id));
                return "success";
            }
            echo "fails";
        }
        public function deleteChiTiet($id){
            $chiTiet = $this->model("ChiTietHoaDon");

            if($chiTiet->deleteChiTiet($id)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }
        public function updateChiTiet($id){
            $chiTiet = $this->model("ChiTietHoaDon");
            $maHoaDon =  $_POST["mahoadon"];
            $maThucUong = $_POST["mathucuong"];  
            $soLuong = $_POST["soluong"];
            if($chiTiet->updateChiTietById($id,$maHoaDon,$maThucUong,$soLuong)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }
    }
?>