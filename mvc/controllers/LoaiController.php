<?php
    class LoaiController extends Controller{
        public function getListLoai(){
            $ban = $this->model("LoaiThucUong");
            //var_dump($ban->getListBan());
            echo json_encode($ban->getListLoai());
        }
        public function addLoai(){
            $ten = "Nước ngọt";
            $loai = $this->model("LoaiThucUong");
            if($loai->addLoai($ten,$ten)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function deleteLoai($id){
            $loai = $this->model("LoaiThucUong");   
            if($loai->deleteLoai($id)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function updateLoai($id){
            $ten = "Nước ngọt 02";
            $loai = $this->model("LoaiThucUong");   
            if($loai->updateLoai($id,$ten)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
    }

?>