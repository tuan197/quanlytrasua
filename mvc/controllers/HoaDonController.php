<?php 
    class HoaDonController extends Controller{
        public function getHoaDonById($id){
            $hd = $this->model("HoaDon");
            echo json_encode($hd->getHoaDonById($id));
        }
        public function insertHoaDon(){
            $hd = $this->model("HoaDon");
            $maBan =  $_POST["maban"];
            $thanhTien = $_POST["thanhtien"];  
            $ngayTao = date('Y/m/d', time());
            if($hd->insertHoaDon($maBan,$ngayTao,$thanhTien,0)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }
        public function updateTinhTrang($id){
            $hd = $this->model("HoaDon");
            if($hd->updateTinhTrang($id)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }

        public function updateHoaDon($id){
            $hd = $this->model("HoaDon");
            $maBan =  $_POST["maban"];
            $thanhTien = $_POST["thanhtien"];  
            $ngayTao = date('Y/m/d', time());
            if($hd->updateHoaDon($id,$maBan,$ngayTao,$thanhTien)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }

        public function deleteHoaDon($id){
            $hd = $this->model("HoaDon");

            if($hd->deleteHoaDon($id)==1){
                echo "success";
                return "success";
            }
            echo "fails";
        }
    }

?>