<?php
    class ThucUongController extends Controller
    {
        public function getAllList()
        {
            $thucUong = $this->model("ThucUong");
            echo json_encode($thucUong->getAllListThucUong());
        }

        public function getListThucUongByLoai($id)
        {
            $thucUong = $this->model("ThucUong");
            echo json_encode($thucUong->getListThucUongByLoai($id));
        }
        public function insertThucUong()
        {
            $thucUong = $this->model("ThucUong");
            $ten = "Trà chanh";
            $loai = 3;
            $gia = 15000;   
            if($thucUong->addThucUong($ten,$gia,$loai)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function deleteThucUong($id)
        {
            $thucUong = $this->model("ThucUong");
            if($thucUong->delete($id)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
        public function updateThucUong($id)
        {
            $ten = "Trà chanh 02";
            $loai = 3;
            $gia = 15000;  
            $thucUong = $this->model("ThucUong");   
            if($thucUong->update($id,$ten,$gia,$loai)==1){
                echo "success";
            }else{
                echo "fails";
            }
        }
    }

?>