<?php

    class Controller{

        public function model($model){
            //$numargs = func_num_args();
            require_once "./mvc/models/".$model.".php";
            return new $model;
        }

        public function view($view,$data = []){
            require_once "./mvc/views/".$view.".php";
        }

    }
?>