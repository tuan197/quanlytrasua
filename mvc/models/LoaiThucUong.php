<?php 
    class LoaiThucUong extends DB{
        public $id;
        public $tenLoai;

        function __construct($id=null,$tenLoai=null) {
            $this->id      = $id;
            $this->tenLoai = $tenLoai;
            parent::__construct();
        }

        public function getListLoai(){
            $sql = "SELECT * FROM loaithucuong";
            $arr = mysqli_query($this->con,$sql);
            $result =  [];
            while($row = mysqli_fetch_assoc($arr)){
                array_push($result,new LoaiThucUong($row["id"],$row["TenLoai"]));
            }
            return $result;
        }
        public function addLoai($tenLoai){
            $sql = "INSERT INTO loaithucuong(TenLoai) VALUES ('$tenLoai')";
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }
        public function deleteLoai($id){
            $sql = "DELETE FROM loaithucuong WHERE id=".$id;
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }

        public function updateLoai($id,$tenLoai){
            $sql = "UPDATE loaithucuong SET TenLoai ='".$tenLoai."' WHERE id= ".$id;
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }
    }
?>