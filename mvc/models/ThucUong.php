<?php 
class ThucUong extends DB{
    public $id;
    public $tenThucUong;
    public $gia;
    public $maLoai;
    public $anh;
    public $tenLoai;

    function __construct($id=null,$tenthucuong=null,$gia=null,$maloai=null,$anh=null,$tenLoai=null) {
        $this->id          = $id;
        $this->tenThucUong = $tenthucuong;
        $this->gia         = $gia;
        $this->maLoai      = $maloai;
        $this->anh         = $anh;
        $this->tenLoai     = $tenLoai;
        parent::__construct();
    }

    public function getAllListThucUong()
    {
        
        $sql = "SELECT * FROM thucuong";
        $arr = mysqli_query($this->con,$sql);
        $result =  [];
        while($row = mysqli_fetch_assoc($arr)){
            array_push($result,new ThucUong($row["id"],$row["TenThucUong"],$row["Gia"],$row["MaLoai"],$row["Anh"],$row["TenLoai"]));
        }
        return $result;
    }
    
    public function getListThucUongByLoai($idLoai)
    {
        $sql = "SELECT * FROM thucuong WHERE MaLoai=".$idLoai;
        $arr = mysqli_query($this->con,$sql);
        $result =  [];
        while($row = mysqli_fetch_assoc($arr)){
            array_push($result,new ThucUong($row["id"],$row["TenThucUong"],$row["Gia"],$row["MaLoai"],$row["Anh"]));
        }
        return $result;
    }
    public function addThucUong($tenThucUong,$gia,$maLoai)
    {
        $sql = "INSERT INTO thucuong(TenThucUong,Gia,MaLoai,Anh) VALUES ('$tenThucUong',$gia,$maLoai,'$anh')";
        //echo $sql;
        if($this->con->query($sql)===true){
            return 1;
        }else{
            echo "lOI : ".$this->con->error;
        }
    }
    public function update($id,$tenThucUong,$gia,$maLoai)
    {
        $sql = "UPDATE thucuong SET TenThucUong ='".$tenThucUong."',Gia=".$gia.",MaLoai=".$maLoai." WHERE id= ".$id;
        if($this->con->query($sql)===true){
            return 1;
        }else{
            echo "lOI : ".$this->con->error;
        }
    }
    public function delete($id)
    {
        $sql = "DELETE FROM thucuong WHERE ID=".$id;
        if($this->con->query($sql)===true){
            return 1;
        }else{
            echo "lOI : ".$this->con->error;
        }
    }
}

?>