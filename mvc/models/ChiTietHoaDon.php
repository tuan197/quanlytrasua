<?php 
    class ChiTietHoaDon extends DB{
        public $id;
        public $maHoaDon;
        public $maThucUong;
        public $soLuong;

        function __construct($id=null,$maHoaDon=null,$maThucUong=null,$soLuong=null) {
            $this->id         = $id;
            $this->maHoaDon   = $maHoaDon;
            $this->maThucUong = $maThucUong;
            $this->soLuong    = $soLuong;
            parent::__construct();
        }

        public function getChiTietByIdHoaDon($idHoaDon){
            $sql = "SELECT * FROM chitiethoadon WHERE MaHoaDon=".$idHoaDon;
            $result = mysqli_query($this->con,$sql);
            $arrResult = array();
            while($row = mysqli_fetch_assoc($result)){
                array_push($arrResult, new ChiTietHoaDon($row["id"],$row["MaHoaDon"],$row["MaThucUong"],$row["SoLuong"]));
            }
            if(count($arrResult) > 0){
                return $arrResult;
            }
            return 0;
        }
        public function getChiTietById($id){
            $sql = "SELECT * FROM chitiethoadon WHERE id=".$id;
            $result = mysqli_query($this->con,$sql);
            while($row = mysqli_fetch_assoc($result)){
                if($row != ""){
                    return new ChiTietHoaDon($row["id"],$row["MaHoaDon"],$row["MaThucUong"],$row["SoLuong"]);
                }
                else{
                    return null;
                }
            }
            
        }
        public function insertChiTiet($maHoaDon,$maThucUong,$soLuong){
            $sql = "INSERT INTO chitiethoadon(MaHoaDon,MaThucUong,SoLuong) VALUES($maHoaDon,$maThucUong,$soLuong)";
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }
        public function updateChiTietById($id,$maHoaDon,$maThucUong,$soLuong){
            $sql = "UPDATE chitiethoadon SET MaHoaDon= $maHoaDon, MaThucUong=$maThucUong,SoLuong = $soLuong  WHERE ID=".$id;
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }
        public function deleteChiTietById($id){
            $sql = "DELETE FROM chitiethoadon WHERE id=".$id;
            if($this->con->query($sql)===true){
                return 1;
            }else{
                echo "lOI : ".$this->con->error;
            }
        }
    }

?>